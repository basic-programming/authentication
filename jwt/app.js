const log = console.log;
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import {router as authRoute} from './authRoute.js';
import 'dotenv/config';
import ('./mongoose.js');

// log(process.env.PORT)
const app = express();

// middleware........
app.use(express.json());
app.use(helmet());
app.use(morgan("common"))

app.use(authRoute);

app.listen(process.env.PORT, () => {
    log('Server is up and running...');
})