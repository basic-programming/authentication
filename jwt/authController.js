const log = console.log;
import { User } from './users.js';

const signup = async (req, res) => {
  const user = new User(req.body);
  try {
      await user.save();
      const token = await user.generateAuthToken();
      res.header('Authorization', "Bearer " + token);
      res.status(201).send({ user, token })
  } catch ( err ) {
      res.status(400).send(err);
  }
}

const login = async(req, res) => {
  try {
      const user = await User.verifyUser(req.body.email, req.body.password);
      const token = await user.generateAuthToken();
      res.send({ user, token });
  } catch(err) {
      res.status(400).send(err);
  }
}

const logoutAll = async(req, res) => {
  try {
      req.user.tokens = [];
      await req.user.save();
      res.send();
  } catch (err){
      res.status(500).send(err);
  }
}

const logout = async(req, res) => {
  try{
      req.user.tokens = req.user.tokens.filter(token => token.token !== req.token);
      await req.user.save();
      res.send();
  } catch(err) {
      res.status(500).send(err);
  }
}

export {
  signup,
  login,
  logoutAll,
  logout
}
