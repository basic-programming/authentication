import express from 'express';
import { auth } from './auth.js';
import { signup,
    login,
    logoutAll,
    logout } from './authController.js';

const router = express.Router();

router.post('/user', signup);

router.post('/user/login', login);

router.post('/user/logoutall', auth, logoutAll);

router.post('/user/logout', auth, logout);

export { router };   