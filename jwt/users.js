const log = console.log;
import mongoose from 'mongoose';
import validator from 'validator';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true,
        required: true,
        unique: true,
        validate(value) {
            if (!(validator.isEmail(value)))
                throw new Error('Email is not valid...');
        }
    },
    password: {
        type: String,
        trim: true,
        required: true,
        minlength: 5
    },
    tokens: [{
        token: {
            type: String
        }
    }]
},
    { timestamps: true }
);

userSchema.virtual('posts', {
    ref: 'Post',
    localField: '_id',
    foreignField: 'userId'
})

userSchema.methods.toJSON = function () {
    const user = this;
    const userObject = user.toObject()

    delete userObject.password;
    delete userObject.tokens;

    return userObject
}

userSchema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET);

    user.tokens = user.tokens.concat({ token })
    await user.save();

    return token;
}

userSchema.statics.verifyUser = async (email, password) => {
    const user = await User.findOne({ email });

    if (!user)
        throw new Error("Unable to login....");
    
        const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch)
        throw new Error("Unable to login....");
    
    return user;
}

userSchema.pre('save', async function (next) {
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
})

const User = mongoose.model('User', userSchema);

export { User };