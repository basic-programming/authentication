import mongoose from 'mongoose';
mongoose.connect(process.env.MONGODB_URL, {}, () => {
    console.log("Server connected to mongodb");
});  