var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(session({
    secret: "Your secret key",
    resave: false,
    saveUninitialized: true,
}));

var Users = [];

app.post('/signup', function(req, res){
    if(!req.body.id || !req.body.password){
      res.status(400).send("Invalid details!");
    } else {
        Users.filter(function(user){
            if(user.id === req.body.id){
                res.status(400).send({ message: "User Already Exists! Login or choose another user id" });
            }
        });
        var newUser = {id: req.body.id, password: req.body.password};
        Users.push(newUser);
        req.session.user = newUser;
        res.redirect('/protected_page');
    }
});
function checkSignIn(req, res, next){
   if(req.session.user){
      next();     //If session exists, proceed to page
   } else {
      var err = new Error("Not logged in!");
      console.log(req.session.user);
      next(err);  //Error, trying to access unauthorized page!
   }
}
app.get('/protected_page', checkSignIn, function(req, res){
   res.send({id: req.session.user.id, msg: "Top Secret"});
});

app.post('/login', function(req, res){
   console.log("users " + Users);
   if(!req.body.id || !req.body.password){
      return res.status(400).send({message: "Please enter both id and password"});;
   } else {
      Users.filter(function(user){
         if(user.id === req.body.id && user.password === req.body.password){
            console.log("it's done...")
            req.session.user = user;
            res.redirect('/protected_page');
            return;
         }
      });
      res.status(400).send({error: "Invalid credentials!"});
   }
});

app.get('/logout', function(req, res){
   req.session.destroy(function(){
      console.log("user logged out.")
   });
   res.redirect('/login');
});

app.use('/protected_page', function(err, req, res, next){
console.log(err);
   //User should be authenticated! Redirect him to log in.
   res.redirect('/login');
});

app.listen(3000, () => {
    console.log("listning on port 3000....")
});